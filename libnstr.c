#include <stdlib.h>
#include <stdarg.h>
#include "nstr.h"

#define likely(x)   __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)

struct nstr_ll_t {
  nstr_t    *str;
  nstr_ll_t *next;
};

void
nstr_ref(
    nstr_t *nstr) {

  if ( likely(nstr->refs < INT_MAX) )
    ++nstr->refs;

}

void
nstr_unref(
    nstr_t *nstr) {

  if ( likely(nstr->refs < INT_MAX) ) {
    --nstr->refs;

    if ( ! nstr->refs ) {

      if ( nstr->type == NSTR_BUF_T ) {

        free(nstr->buf);

      } else if (nstr->type == NSTR_LIST_T) {

        nstr_ll_t *end = nstr->end;
        nstr_ll_t *cur = end->next;

        do {

          nstr_unref(cur->str);
          nstr_ll_t *n = cur->next;
          free(cur);
          cur = n;

        } while( cur != end );

      }

      free(nstr);
    }

  }

}

nstr_t *
nstr_new_const(
    const char *cstr,
    size_t      len) {

  nstr_t *ret = malloc(sizeof(*ret));

  if ( likely(ret != 0) ) {
    ret->type = NSTR_CONST_T;
    ret->refs = 1;
    ret->len  = len;
    ret->cstr = cstr;
  }

  return ret;

}

nstr_t *
nstr_new_buf(
    char   *buf,
    size_t  len) {

  nstr_t *ret = malloc(sizeof(*ret));

  if ( likely(ret != 0) ) {
    ret->type = NSTR_BUF_T;
    ret->refs = 1;
    ret->len  = len;
    ret->buf  = buf;
  }

  return ret;

}

static void
nstr_copy_lst_to_buf(
    char   *buf,
    nstr_t *nstr) {

  nstr_ll_t *end = nstr->end;
  nstr_ll_t *cur = end->next;

  do {

    nstr_t *str = cur->str;
    nstr_copy_to_buf(buf, str);
    buf += str->len;

    cur = cur->next;

  } while( cur != end );

}

void
nstr_copy_to_buf(
    char   *buf,
    nstr_t *nstr) {

  if ( nstr->type != NSTR_LIST_T ) {

    memcpy(buf, nstr->cstr, nstr->len);

  } else {

    nstr_copy_lst_to_buf(buf, nstr);

  }

}

void
nstr_copy_cstr_to_buf(
    char   *buf,
    nstr_t *nstr) {

  nstr_copy_to_buf(buf, nstr);
  buf[nstr->len] = 0;

}

char *
nstr_get_cstr_buf(
    nstr_t *nstr) {

  char *ret = malloc(nstr->len + 1);

  if ( likely(ret != 0) ) {

    nstr_copy_cstr_to_buf(ret, nstr);

  }

  return ret;

}

static nstr_t *
nstr_fill_lst(
    nstr_t *ret,
    va_list ap) {

  nstr_ll_t *end   = ret->end;
  nstr_ll_t *start = end->next;
  nstr_t *s;

  while( (s = va_arg(ap, nstr_t *)) ) {

    nstr_ll_t *n = malloc(sizeof(*n));

    if ( unlikely(n != 0) )
      goto err;

    n->str = s;

    end->next = n;
    end = n;

    ret->len += s->len;

  }

  end->next = start;
  ret->end = end;

end:
  return ret;

err:
  end->next = start;
  ret->end = end;

  nstr_unref(ret);
  ret = 0;
  goto end;

}

static nstr_t *
nstr_new_lst(
    nstr_t *a,
    nstr_t *b) {

  nstr_t *ret = malloc(sizeof(*ret));

  if ( unlikely(ret == 0) )
    goto end;

  nstr_ll_t *la = malloc(sizeof(*la));

  if ( unlikely(la == 0 ) )
    goto err_new_la;

  nstr_ll_t *lb = malloc(sizeof(*lb));

  if ( unlikely(lb == 0) )
    goto err_new_lb;

  la->str  = a;
  la->next = lb;

  lb->str  = b;
  lb->next = la;

  ret->type = NSTR_LIST_T;
  ret->len  = a->len + b->len;
  ret->end  = lb;

end:
  return ret;

err_new_lb:
  free(la);

err_new_la:
  free(ret);

  ret = 0;
  goto end;

}


nstr_t *
nstr_merge(
    nstr_t *a,
    nstr_t *b,
    ...) {

  nstr_t *ret;

  if ( unlikely( a == 0 || b == 0 ) ) {
    ret = a;
    goto end;
  }

  if ( a->type == NSTR_LIST_T && a->refs == 1 ) {

    ret = a;

  } else {

    ret = nstr_new_lst(a, b);
    if ( unlikely(ret == 0) )
      goto end;

  }

  va_list ap;
  va_start(ap, b);

  ret = nstr_fill_lst(ret, ap);

  va_end(ap);

end:
  return ret;

}
