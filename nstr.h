#ifndef NSTR_H
#define NSTR_H

#include <limits.h>

typedef struct nstr_ll_t nstr_ll_t;
typedef struct nstr_t nstr_t;

struct nstr_t {
  enum {
    NSTR_CONST_T,
    NSTR_BUF_T,
    NSTR_LIST_T
  } type;

  int    refs;
  size_t len;

  union {
    const char        *cstr;
          char        *buf;
          nstr_ll_t   *end;
  };

};

#define nstr_const(str) { NSTR_CONST_T, INT_MAX, sizeof(str), {(str)} }

nstr_t *
nstr_new_const(
    const char *buf,
    size_t      len);

nstr_t *
nstr_new_buf(
    char   *buf,
    size_t  len);

void
nstr_ref(
    nstr_t *nstr);

void
nstr_unref(
    nstr_t *nstr);

// merge the strings together and returns the merged result
// new string takes over the ownship of the substrings
// so if you want to user the string somewhere else, use nstr_concat
nstr_t *
nstr_merge(
    nstr_t *a,
    nstr_t *b,
    ...);

// concatenate the string. It will reference the arguments so they
// can be used further and one still have to unreference them
nstr_t *
nstr_concat(
    nstr_t *a,
    nstr_t *b,
    ...);

void
nstr_copy_to_buf(
    char   *buf,
    nstr_t *nstr);

void
nstr_copy_cstr_to_buf(
    char   *buf,
    nstr_t *nstr);

char *
nstr_get_cstr_buf(
    nstr_t *nstr);

#endif // NSTR_H
