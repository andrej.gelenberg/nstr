#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "nstr.h"

static void
check_test_str(
    nstr_t *str) {

  assert(str->len = sizeof("test") - 1);
  char cstr[sizeof("test")];

  nstr_copy_cstr_to_buf(
      cstr,
      str);

  assert(strcmp("test", cstr) == 0);

  nstr_unref(str);

}

static void
run_cnst_test() {
  nstr_t s = nstr_const("test");
  check_test_str(&s);
}


int
main(
    int argc,
    const char **argv) {

#define run_test(test) \
  printf("run test " # test "\n"); \
  run_ ## test ## _test();

  printf("start running tests\n\n");

  run_test(cnst);

  printf("\nend running tests\n");

  return 0;
}
