PROJECT_NAME := libnstr
AUTHOR := Andrej Gelenberg <andrej.gelenberg@udo.edu>
VER_MAJOR := 0
VER_MINOR := 1

all: libnstr.so

libnstr.so: libnstr.o
libnstr.so: LIBS=

test/test: libnstr.so.$(VER_MAJOR)
test/test: test/test.o
test/test: LDFLAGS=-L.
test/test: LIBS=-lnstr

CFLAGS += -I.

CLEAN += libnstr.so test/test tset/*.txt

test/test libnstr.so: Makefile

$(eval $(call install_so,libnstr.so))
install: $(DESTDIR)/usr/include/nstr.h

test/test.txt: test/test
	LD_LIBRARY_PATH="$$LD_LIBRARY_PATH:." '$<' | tee '$@' || (rm '$@'; false)


